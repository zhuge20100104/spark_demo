import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}

import java.sql.Timestamp
import java.time.LocalDateTime
import scala.util.Random

object ClusterDataGen {

  def main(args: Array[String]): Unit = {

    // 创建SparkSession
    val spark = SparkSession.builder()
      .appName("Cluster Data Generation")
      .master("local[*]")  // 本地模式，实际使用时可配置为集群模式
      .getOrCreate()

    // 定义数据结构
    val schema = StructType(Array(
      StructField("cluster_id", IntegerType, nullable = false),
      StructField("featured_ids", ArrayType(IntegerType), nullable = false),
      StructField("cluster_center", ArrayType(DoubleType), nullable = false),
      StructField("cluster_label", StringType, nullable = true),
      StructField("last_modified_date", TimestampType, nullable = false)
    ))

    // 生成随机数据
    // 5000
    val numRecords = 5000 // 生成5000个记录
    val random = new Random()

    // 获取当前时间
    val currentTime = LocalDateTime.now()
    val numPartitions = 10
    val partitionEleCount = numRecords / numPartitions

    // 1000可以work
    val aClusterFeatureIdxRange = 1000
    // 20000
    // 使用RDD生成数据
    val dataRDD = spark.sparkContext.parallelize(1 to numRecords, numPartitions)  // 使用10个分区
    val data = dataRDD.mapPartitionsWithIndex { (partitionIdx, partitionData) =>

      val partitionFeatureSize = partitionEleCount * aClusterFeatureIdxRange
      val startIndex = partitionIdx * partitionFeatureSize
      // 其实应该是减1， 这样是为了故意的制造Duplicated Featured Ids
      val endIndex = (partitionIdx + 1) * partitionFeatureSize  + 3
      val partitionAllFeatureIds =  (startIndex to endIndex).toList
      var shuffledFeatureIds = Random.shuffle(partitionAllFeatureIds)  // 打乱顺序

      partitionData.map { id =>
        val clusterId = id

        // 随机生成 featured_ids 的大小，范围在 1 到 20000 之间
        val size = random.nextInt(aClusterFeatureIdxRange) + 1 // 确保至少为1
        val featuredIds = shuffledFeatureIds.take(size).toArray // 从打乱后的列表中选择不重复的ID
        shuffledFeatureIds = shuffledFeatureIds.drop(size)

        val clusterCenter = {
          val vector = Array.fill(256)(random.nextDouble())
          val norm = Math.sqrt(vector.map(x => x * x).sum) // 计算L2范数
          vector.map(_ / norm) // 归一化
        }
        val clusterLabel = s"Cluster $id"

        // 随机生成时间戳
        val randomDays = random.nextInt(766) // 随机生成0到765天
        val randomDate = currentTime.minusDays(randomDays.toLong)
        val lastModifiedDate = Timestamp.valueOf(randomDate)

        Row(clusterId, featuredIds, clusterCenter, clusterLabel, lastModifiedDate)
      }
    }

    // 创建DataFrame
    val df = spark.createDataFrame(data, schema)

    // 显示数据的部分内容
    df.show(truncate = false)

    // 写入Hive表（假设已连接Hive）
    df.write.mode("overwrite").parquet("/user/hive/warehouse/cluster_data_24602")

    spark.stop()

  }
}