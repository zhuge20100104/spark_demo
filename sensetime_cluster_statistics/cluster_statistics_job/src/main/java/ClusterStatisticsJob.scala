import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, DoubleType, IntegerType, StringType, StructField, StructType, TimestampType}

object ClusterStatisticsJob extends Serializable {

  val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    // 创建SparkSession
    val spark = SparkSession.builder()
      .appName("Cluster Data Generation")
      .master("local[*]")  // 本地模式，实际使用时可配置为集群模式
      .getOrCreate()

    import spark.implicits._

    logger.setLevel(Level.INFO)
    val dateTimeStr = DateUtils.getCurrentDateTimeStr()

    // 定义数据结构
    val schema = StructType(Array(
      StructField("cluster_id", IntegerType, nullable = false),
      StructField("featured_ids", ArrayType(IntegerType), nullable = false),
      StructField("cluster_center", ArrayType(DoubleType), nullable = false),
      StructField("cluster_label", StringType, nullable = true),
      StructField("last_modified_date", TimestampType, nullable = false)
    ))

    val rawDF = spark.read.schema(schema).parquet("/user/hive/warehouse/cluster_data_24602")

    if(rawDF.count() == 0) {
      logger.info("No raw clusters data arrived, quit job...")
      return
    }

    // Handle cluster center size business
    val clusterDF = rawDF.filter("last_modified_date>'2023-01-01'")
    if(clusterDF.count() == 0) {
      logger.info("No valid clusters data arrived, quit job...")
      return
    }

    val clusterWithClassDescDF = clusterDF.withColumn("class_description",
        when(size(col("featured_ids")) > 800, struct(lit("800+").alias("class"), lit("超大类").alias("description")))
        .when(size(col("featured_ids")) > 600, struct(lit("600-800").alias("class"), lit("大类").alias("description")))
        .when(size(col("featured_ids")) > 300, struct(lit("300-600").alias("class"), lit("中等类").alias("description")))
        .otherwise(struct(lit("1-300").alias("class"), lit("正常类").alias("description")))
    )

    val clusterDescGroupedDF = clusterWithClassDescDF
      .select(
        col("class_description.class").alias("class"),
        col("class_description.description").alias("description")
      )
      .groupBy("class", "description")
      .agg(count("*").alias("count"))

    val clusterCenterFinalDF = clusterDescGroupedDF.coalesce(1).withColumn("id", monotonically_increasing_id())
      .orderBy(col("id").asc)

    val resultClusterCenterSizeCSV = s"/user/result/result_cluster_center_size_$dateTimeStr"
    clusterCenterFinalDF.write
      .mode("overwrite")
      .option("header", "true")
      .csv(resultClusterCenterSizeCSV)


    // Handle time_range related business
    val clusterTimeRangeDF = clusterDF.select(lit(1).as("id"),
        min("last_modified_date").as("min_modify_date"),
        max("last_modified_date").as("max_modify_date")
    )

    val resultClusterTimeRangeCSV = s"/user/result/result_cluster_time_range_$dateTimeStr"
    clusterTimeRangeDF.write
      .mode("overwrite")
      .option("header", "true")
      .csv(resultClusterTimeRangeCSV)


    // Handle cluster count per month logic
    // 提取年份和月份，合并成一个字符串列
    val dfWithMonth = clusterDF.withColumn("date_time", date_format(col("last_modified_date"), "yyyy-MM"))

    // 按年月份分组统计 cluster_id 的个数
    val monthlyCount = dfWithMonth.groupBy("date_time")
      .agg(countDistinct("cluster_id").alias("cluster_count"))

    val clusterCountPerMonthDF = monthlyCount.coalesce(1)
      .select(col("date_time"), col("cluster_count")).orderBy(col("date_time").asc)
      .withColumn("id", monotonically_increasing_id())
      .select(col("id"), col("date_time"), col("cluster_count"))

    val resultClusterCountPerMonthCSV = s"/user/result/result_cluster_count_per_month_$dateTimeStr"
    clusterCountPerMonthDF.write
      .mode("overwrite")
      .option("header", "true")
      .csv(resultClusterCountPerMonthCSV)

    // Handle duplicate feature_ids
    // 展开 featured_ids 列
    val explodedDf = clusterDF
      .select(col("cluster_id"), col("featured_ids"))
      .withColumn("featured_id", explode(col("featured_ids")))

    // 按 featured_id 分组，统计 cluster 个数和 cluster_ids 列表
    val duplicateFeaturedDF = explodedDf.groupBy("featured_id")
      .agg(
        countDistinct("cluster_id").alias("cluster_count"),
        collect_set("cluster_id").alias("cluster_ids")
      )
      .filter(col("cluster_count") > 1) // 只保留重复的 featured_id

    val duplicateFinalDF = duplicateFeaturedDF.coalesce(1)
      .withColumn("id", monotonically_increasing_id())
      .orderBy(col("id").asc)
      .select(col("id"), col("featured_id"), col("cluster_count"), col("cluster_ids").cast("string"))

    val resultDuplicateFeaturedIDCSV = s"/user/result/result_duplicate_featured_id_$dateTimeStr"
    duplicateFinalDF.write
      .mode("overwrite")
      .option("header", "true")
      .csv(resultDuplicateFeaturedIDCSV)

    spark.stop()
  }
}