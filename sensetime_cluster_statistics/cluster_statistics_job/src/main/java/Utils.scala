import java.time.{LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter

object DateUtils {
  def getCurrentDateTimeStr(): String = {
    val currentDateTime = LocalDateTime.now()
    // 定义格式化器
    val formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss")
    // 格式化当前日期时间
    val formattedDateTime = currentDateTime.format(formatter)
    return formattedDateTime
  }
}
