import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.conf.Configuration

object HdfsUtils {

  def makeHivePath(pathName: String): String = {
    val hdfsPath = "hdfs://namenode:9000/user/hive/warehouse/" + pathName
    return hdfsPath
  }

  /**
   * 检查指定 HDFS 目录是否为空
   *
   * @param hdfsPath HDFS 目录路径
   * @return 如果目录为空返回 true，否则返回 false
   */
  def isDirectoryEmpty(hdfsPath: String): Boolean = {
    // 创建 Hadoop 配置
    val conf = new Configuration()
    val fs = FileSystem.get(conf)
    val path = new Path(hdfsPath)

    try {
      if (fs.exists(path) && fs.isDirectory(path)) {
        // 获取目录下的所有文件和子目录
        val fileStatuses = fs.listStatus(path)

        // 检查目录是否为空
        fileStatuses.isEmpty
      } else {
        throw new IllegalArgumentException(s"路径 $hdfsPath 不存在或不是目录")
      }
    } catch {
      case e: Exception =>
        // 捕获并打印异常信息
        println(s"发生错误: ${e.getMessage}")
        false
    } finally {
      // 关闭 Hadoop 文件系统
      fs.close()
    }
  }
}