import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}


object SparkETLJob extends Serializable {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("SparkETLJob")
      .master("local[*]")
      .enableHiveSupport()
      .config("spark.cassandra.connection.host", "cassandra.big-data-cluster_default")
      .config("spark.cassandra.connection.port", "9042")
      .getOrCreate()


    import spark.implicits._

    // Kafka 消息的 schema 定义
    val keySchema = StructType(Array(
      StructField("latitude", DoubleType, nullable = true),
      StructField("longtitude", DoubleType, nullable = true)
    ))

    val valueSchema = StructType(Array(
      StructField("version", StringType, nullable = true),
      StructField("region", StringType, nullable = true),
      StructField("country", StringType, nullable = true),
      StructField("timestamp", StringType, nullable = true),
      StructField("max_speed_limit", DoubleType, nullable = true),
      StructField("cur_road_curv", DoubleType, nullable = true),
      StructField("rain_speed_limit", DoubleType, nullable = true),
      StructField("snow_speed_limit", DoubleType, nullable = true),
      StructField("fog_speed_limit", DoubleType, nullable = true),
      StructField("real_time_speed_limit", DoubleType, nullable = true)
    ))

    // 从 Kafka 读取数据
    val kafkaStream = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "kafka-broker:29092")
      .option("subscribe", "map_log_101")
      .option("startingOffsets", "earliest")
      .option("maxOffsetsPerTrigger", 1000) // 每次触发处理的最大偏移量数
      .load()

    // 提取键和值，并解析 JSON
    val keyValueStream = kafkaStream
      .selectExpr("CAST(key AS STRING) AS key", "CAST(value AS STRING) AS value")


    // 解析 key 和 value 的 JSON 数据
    val parsedStream = keyValueStream
      .select(
        from_json(col("key"), keySchema).as("key_data"),
        from_json(col("value"), valueSchema).as("value_data")
      )
      .select(
        col("key_data.latitude"),
        col("key_data.longtitude"),
        col("value_data.version"),
        col("value_data.region"),
        col("value_data.country"),
        col("value_data.timestamp"),
        col("value_data.max_speed_limit"),
        col("value_data.cur_road_curv"),
        col("value_data.rain_speed_limit"),
        col("value_data.snow_speed_limit"),
        col("value_data.fog_speed_limit"),
        col("value_data.real_time_speed_limit")
      )


    val metricsParseQuery = parsedStream.writeStream
      .outputMode("append")
      .foreachBatch { (batchDF: DataFrame, _: Long) =>
        // rawDataDF handling logic
        batchDF.write.mode("append").parquet("/user/hive/warehouse/raw_data_101")

        // CountryDF handling logic
        val countryMappedDF = batchDF.select("region", "country")
          .distinct()
          .withColumn("country_id", expr("uuid()"))
          .select("country_id", "region", "country")

        countryMappedDF.write
          .mode("overwrite")
          .parquet("/user/country_mapping")

        val countryDF = spark.read.parquet("/user/country_mapping")

        countryDF.write
          .mode("append")
          .parquet("/user/hive/warehouse/country_101")

        countryDF.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map("keyspace" -> "map_1_0_1", "table" -> "country")) // 替换为你的 keyspace 和 table
          .mode("append") // 可以是 "append", "overwrite" 等
          .save()

        // TimestampDF handling Logic
        val timestampMappedDF = batchDF.select("timestamp")
          .withColumn("date", date_format(col("timestamp"), "yyyy-MM-dd"))
          .withColumn("month", date_format(col("timestamp"), "yyyy-MM"))
          .withColumn("date_id", expr("uuid()"))
          .select("date_id", "date", "month", "timestamp")

        timestampMappedDF.write
          .mode("overwrite")
          .parquet("/user/timestamp_mapping")

        val timestampDF = spark.read.parquet("/user/timestamp_mapping")

        timestampDF.write.mode("append").parquet("/user/hive/warehouse/date_time_101")

        timestampDF.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map("keyspace" -> "map_1_0_1", "table" -> "date_time")) // 替换为你的 keyspace 和 table
          .mode("append") // 可以是 "append", "overwrite" 等
          .save()

        // GeoDF handling logic
        val geoMappedDF = batchDF.select("latitude", "longtitude")
          .withColumn("geo_id", expr("uuid()"))
          .select("geo_id", "latitude", "longtitude")

        geoMappedDF.write
          .mode("overwrite")
          .parquet("/user/geo_mapping")

        val geoDF = spark.read.parquet("/user/geo_mapping")

        geoDF.write.mode("append").parquet("/user/hive/warehouse/geo_location_101")

        geoDF.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map("keyspace" -> "map_1_0_1", "table" -> "geo_location")) // 替换为你的 keyspace 和 table
          .mode("append") // 可以是 "append", "overwrite" 等
          .save()

        // LogMetricsDF handling logic
        val logMetricsMappedDF = batchDF
          .join(countryDF, Seq("region", "country"))
          .join(timestampDF, "timestamp")
          .join(geoDF, Seq("latitude", "longtitude"))
          .withColumn("id", expr("uuid()"))
          .select(
            col("id"),
            col("geo_id"),
            col("country_id"),
            col("date_id"),
            col("max_speed_limit"),
            col("cur_road_curv"),
            col("rain_speed_limit"),
            col("snow_speed_limit"),
            col("fog_speed_limit"),
            col("real_time_speed_limit")
          )


        logMetricsMappedDF.write
          .mode("overwrite")
          .parquet("/user/log_metrics_mapping")

        val logMetricsDF = spark.read.parquet("/user/log_metrics_mapping")

        logMetricsDF.write.mode("append").parquet("/user/hive/warehouse/log_metrics_101")

        logMetricsDF.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map("keyspace" -> "map_1_0_1", "table" -> "log_metrics")) // 替换为你的 keyspace 和 table
          .mode("append") // 可以是 "append", "overwrite" 等
          .save()

      }
      .option("checkpointLocation", "/user/metrics_parse_query_101_checkpoint_5")
      .trigger(Trigger.ProcessingTime("3 seconds"))
      .start()

    metricsParseQuery.awaitTermination()
  }
}