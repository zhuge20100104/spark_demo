import java.time.{LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter

object DateUtils {
  def getCurrentDateTimeStr(): String = {
    val currentDateTime = LocalDateTime.now()
    // 定义格式化器
    val formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss")
    // 格式化当前日期时间
    val formattedDateTime = currentDateTime.format(formatter)
    return formattedDateTime
  }

  def getCurrentDateMinus8Str(): String = {
    val currentDate = LocalDate.now()
    val dateMinus8Days = currentDate.minusDays(8)
    val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val dateMinus8DaysString = dateMinus8Days.format(dateFormatter)
    return dateMinus8DaysString
  }

}
