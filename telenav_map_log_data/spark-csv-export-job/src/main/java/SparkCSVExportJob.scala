import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}


object SparkCSVExportJob extends Serializable {

  val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("SparkETLJob")
      .master("local[*]")
      .getOrCreate()

    logger.setLevel(Level.INFO)
    val applicationId = spark.sparkContext.applicationId
    logger.info(s"Start Spark application: $applicationId")

    // 定义模式
    val logMetricsSchema = StructType(Array(
      StructField("id", StringType, true),
      StructField("geo_id", StringType, true),
      StructField("country_id", StringType, true),
      StructField("date_id", StringType, true),
      StructField("max_speed_limit", DoubleType, true),
      StructField("cur_road_curv", DoubleType, true),
      StructField("rain_speed_limit", DoubleType, true),
      StructField("snow_speed_limit", DoubleType, true),
      StructField("fog_speed_limit", DoubleType, true),
      StructField("real_time_speed_limit", DoubleType, true)
    ))
    val rawMetricsDF = spark.read.schema(logMetricsSchema).parquet("/user/hive/warehouse/log_metrics_101")
    if(rawMetricsDF.count == 0) {
      logger.info("No log metrics detected, exit job!!!")
      return
    }


    val countryDF = spark.read.parquet("/user/hive/warehouse/country_101")
    val geoDF = spark.read.parquet("/user/hive/warehouse/geo_location_101")

    // 过滤最近8天之内的数据，然后开始计算
    val dateTimeDF = spark.read.parquet("/user/hive/warehouse/date_time_101")
    val currentDateMinus8Str = DateUtils.getCurrentDateMinus8Str()

    val logMetricsDF = rawMetricsDF.join(dateTimeDF, Seq("date_id"), "inner")
      .join(countryDF, Seq("country_id"), "left")
      .join(geoDF, Seq("geo_id"), "left")
      .filter(to_date(col("date"), "yyyy-MM-dd") >= lit(currentDateMinus8Str))
      .select("id", "country", "region", "latitude", "longtitude", "max_speed_limit", "cur_road_curv")

    // TODO: 上面把countryDF也join一下, partitionBy (country, region) 就行了
    val windowSpec = Window.partitionBy("country", "region").orderBy(col("max_speed_limit").desc)
    // 为每个国家的行分配排名
    val dfWithRank = logMetricsDF.withColumn("rank", row_number().over(windowSpec))
    // 选择排名为 1 的行，即每个国家的最大 max_speed_limit
    val resultMaxSpeedLimitDF = dfWithRank.filter(col("rank") === 1)
      .select("id", "region", "country", "latitude", "longtitude", "max_speed_limit")

    val dateTimeStr = DateUtils.getCurrentDateTimeStr()
    val resultMaxSpeedLimitCSV = s"/user/result/result_max_speed_limit_$dateTimeStr"
    resultMaxSpeedLimitDF.coalesce(1)
      .write
      .mode("overwrite")
      .option("header", "true")
      .csv(resultMaxSpeedLimitCSV)

    val maxRoadCurvWindowSpec = Window.partitionBy("country", "region").orderBy(col("cur_road_curv").desc)
    // 为每个国家的行分配排名
    val maxRoadCurvDfWithRank = logMetricsDF.withColumn("rank", row_number().over(maxRoadCurvWindowSpec))
    val resultMaxRoadCurvDF = maxRoadCurvDfWithRank.filter(col("rank") === 1)
      .select("id", "region", "country", "latitude", "longtitude", "cur_road_curv")

    val resultMaxRoadCurvCSV = s"/user/result/result_max_road_curv_$dateTimeStr"
    resultMaxRoadCurvDF.coalesce(1)
      .write
      .mode("overwrite")
      .option("header", "true")
      .csv(resultMaxRoadCurvCSV)

    logger.info(s"Done Spark application: $applicationId")
  }
}