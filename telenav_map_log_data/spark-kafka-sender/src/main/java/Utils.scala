import play.api.libs.json.{Format, JsValue, Json}

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.util.Random
import scala.collection.mutable

case class Location(latitude: Double, longtitude: Double)
case class Metrics(version:String, region: String, country: String, timestamp: String,
                   max_speed_limit: Double, cur_road_curv: Double , rain_speed_limit: Double,
                   snow_speed_limit: Double, fog_speed_limit: Double,
                   real_time_speed_limit: Double)

object NumberGen {
  def generateRandomNumbers(count: Int, min: Double, max: Double): Seq[Double] = {
    require(count > 0, "Count of numbers must be greater than zero")
    require(min < max, "Min value must be less than max value")

    val random = new Random()
    (1 to count).map(_ => random.nextDouble() * (max - min) + min)
  }

  def generateRandomIndex(min: Int, max: Int): Int = {
    val random = new Random

    // 创建一个包含范围内所有整数的 Range
    val range = min to max

    // 从 Range 中随机选择一个整数
    val randomInt = range(random.nextInt(range.size))
    return randomInt
  }
}

object DateTimeUtil {
  def getCurrentFormattedDateTime(pattern: String): String = {
    val now = LocalDateTime.now() // 获取当前的本地日期时间
    val formatter = DateTimeFormatter.ofPattern(pattern) // 创建日期时间格式化器
    now.format(formatter) // 格式化当前日期时间
  }
}

object RegionGen {
  val regionSeq = Seq[String]("EU", "NA")

  var regionCountryMap = mutable.Map[String, Seq[String]](
    "EU" -> Seq[String]("GB", "SE", "IE", "FR", "DE"),
    "NA" -> Seq[String]("CA", "US", "MX", "CU", "HT")
  )
  def getRegionAndCountry(): (String, String) = {
    val regionIdx = NumberGen.generateRandomIndex(0, regionSeq.size-1)
    val region = regionSeq(regionIdx)
    val countryIdx = NumberGen.generateRandomIndex(0, regionCountryMap(region).size-1)

    val country = regionCountryMap(region)(countryIdx)
    return (region, country)
  }
}


object KeyValueMaker {
  def makeKey(): String = {
    implicit val locFormat: Format[Location] = Json.format[Location]
    val locNumers = NumberGen.generateRandomNumbers(2, 20.00, 42.00)
    val loc = Location(locNumers(0), locNumers(1))
    val json: JsValue = Json.toJson(loc)
    val jsKeyStr = Json.stringify(json)
    return jsKeyStr
  }

  def makeValue(): String = {
    implicit val metFormat: Format[Metrics] = Json.format[Metrics]
    val metricsNumbers = NumberGen.generateRandomNumbers(6, 3.0, 50.0)
    val dateTimeStr = DateTimeUtil.getCurrentFormattedDateTime("yyyy-MM-dd HH:mm:ss")

    val regionAndCountry = RegionGen.getRegionAndCountry()
    val region = regionAndCountry._1
    val country = regionAndCountry._2

    val metrics = Metrics("1.0.1", region, country, dateTimeStr,
      metricsNumbers(0), metricsNumbers(1), metricsNumbers(2),
      metricsNumbers(3), metricsNumbers(4), metricsNumbers(5))


    val metJson: JsValue = Json.toJson(metrics)
    val metJsValueStr = Json.stringify(metJson)
    return metJsValueStr
  }
}