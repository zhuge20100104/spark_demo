import org.apache.spark.sql.SparkSession

import java.io.Serializable
import scala.collection.mutable


object SparkDemo extends Serializable {

  def main(args: Array[String]): Unit = {
      val spark = SparkSession.builder()
        .master("local[*]")
        .getOrCreate()

      import spark.implicits._
      var count: Int = 0
      while(count < 1000) {

         val arrayBuf = mutable.ArrayBuffer[(String, String)]()
         for(i <- 0 until 5) {
            val key = KeyValueMaker.makeKey()
            val value = KeyValueMaker.makeValue()
            arrayBuf += ((key, value))

         }
        val df = arrayBuf.toDF("key", "value")
        val kafkaBrokers = "kafka-broker:29092"  // 替换为你的 Kafka broker 地址
        val topic = "map_log_101"  // 替换为你的 Kafka topic 名称

        // 将 DataFrame 写入 Kafka
        df
          .selectExpr("CAST(key AS STRING) AS key", "CAST(value AS STRING) AS value")
          .write
          .format("kafka")
          .option("kafka.bootstrap.servers", kafkaBrokers)
          .option("topic", topic)
          .save()

        Thread.sleep(10)
        count += 1
      }

    spark.stop()
  }
}