import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import java.sql.Timestamp

class IDUtils(spark: SparkSession) {
  def getStartAndEndClusterID(currentCount: Int): Tuple2[Long, Long] = {
    // 定义 schema
    val clusterIdSchema = StructType(Array(
      StructField("cluster_id", LongType, true),
      StructField("timestamp", TimestampType, true)
    ))

    // 读取 Parquet 文件
    val clusterIDDf: DataFrame = spark.read
      .schema(clusterIdSchema)
      .parquet("/user/hive/warehouse/latest_cluster_id")

    var startClusterId: Long = 1
    if(clusterIDDf.count  != 0) {
      // 求取当前ClusterID的最大值，作为新的ClusterID
      clusterIDDf.createOrReplaceTempView("latest_cluster_id")
      startClusterId = spark.sql("select max(cluster_id) from latest_cluster_id")
        .head().getAs[Long](0)
    }

    return (startClusterId+1, startClusterId + currentCount)
  }


  def getStartAndEndFeatureID(currentCount: Int): Tuple2[Long, Long] = {
    val featureIdSchema =
      StructType(Array(
        StructField("feature_id", LongType, true),
        StructField("timestamp", TimestampType, true)
      ))

    val featureIDDf: DataFrame = spark.read
      .schema(featureIdSchema)
      .parquet("/user/hive/warehouse/latest_feature_id")

    var startFeatureId: Long = 1
    if(featureIDDf.count != 0) {
      featureIDDf.createOrReplaceTempView("latest_feature_id")
      startFeatureId = spark.sql("select max(feature_id) from latest_feature_id")
        .head().getAs[Long](0)
    }

    return (startFeatureId+1, startFeatureId + currentCount)
  }

  def writeCurrentMaxFeatureID(maxFeatureID: Long): Unit =  {
    // 定义 schema
    val schema = StructType(Array(
      StructField("feature_id", LongType, true),
      StructField("timestamp", TimestampType, true)
    ))

    // 创建当前时间戳
    val currentTimestamp = Timestamp.from(java.time.Instant.now())

    // 创建一行数据，使用当前时间戳
    val data = Seq(Row(maxFeatureID, currentTimestamp))
    // 创建 DataFrame
    val featDF = spark.createDataFrame(spark.sparkContext.parallelize(data), schema)

    // 写入 Parquet 文件，使用 append 模式
    featDF.write
      .mode("append") // 使用 append 模式
      .parquet("/user/hive/warehouse/latest_feature_id")

  }

  def writeCurrentMaxClusterID(maxClusterID: Long): Unit =  {
    // 定义 schema
    val schema = StructType(Array(
      StructField("cluster_id", LongType, true),
      StructField("timestamp", TimestampType, true)
    ))

    // 创建当前时间戳
    val currentTimestamp = Timestamp.from(java.time.Instant.now())

    // 创建一行数据，使用当前时间戳
    val data = Seq(Row(maxClusterID, currentTimestamp))
    // 创建 DataFrame
    val featDF = spark.createDataFrame(spark.sparkContext.parallelize(data), schema)

    // 写入 Parquet 文件，使用 append 模式
    featDF.write
      .mode("append") // 使用 append 模式
      .parquet("/user/hive/warehouse/latest_cluster_id")

  }
}
