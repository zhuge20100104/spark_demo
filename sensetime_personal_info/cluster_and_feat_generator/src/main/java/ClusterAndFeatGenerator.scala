import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.util.Random

object ClusterAndFeatGenerator {
  def main(argc: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .master("local[*]")
      .getOrCreate()

    val idUtils = new IDUtils(spark)
    val clusterStartAndEndIds = idUtils.getStartAndEndClusterID(1000)
    val featStartAndEndIds = idUtils.getStartAndEndFeatureID(2000)

    idUtils.writeCurrentMaxFeatureID(featStartAndEndIds._2)
    idUtils.writeCurrentMaxClusterID(clusterStartAndEndIds._2)

    // Handle features Data to Kafka
    val newFeatureIds = (featStartAndEndIds._1 to featStartAndEndIds._2).toList
    val featureData = spark.sparkContext.parallelize(newFeatureIds).map { featureId =>
      val featureVector = Array.fill(256)(Random.nextFloat()) // 生成 256 维随机向量
      val norm = Math.sqrt(featureVector.map(x => x * x).sum)
      val normalizedVector = featureVector.map(value => value / norm)
      val currentTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now())
      Row(
        featureId,
        "add_feature",
        24602,
        normalizedVector, // 直接使用数组
        "1",
        currentTimestamp, // 使用当前时间戳
        s"s3://image-simple-path/image_$featureId.png"
      )
    }

    // 定义 schema
    val featureSchema = StructType(List(
      StructField("feature_id", LongType, false),
      StructField("message_type", StringType, false),
      StructField("feature_version", IntegerType, false),
      StructField("feature", ArrayType(FloatType), false), // 使用 ArrayType
      StructField("extra_info", StringType, false),
      StructField("timestamp", StringType, false),
      StructField("image_path", StringType, false)
    ))

    val featureDF = spark.createDataFrame(featureData, featureSchema)
    // 将 DataFrame 转换为 JSON 格式并添加 Kafka 需要的 key
    val kafkaFeatureDF = featureDF.select(
      col("feature_id"),
      to_json(struct("*")).as("value") // 将整行转换为 JSON
    )

    // 写入 Kafka
    kafkaFeatureDF.write
      .format("kafka")
      .option("kafka.bootstrap.servers", "kafka-broker:29092") // 替换为你的 Kafka 服务器
      .option("topic", "feature_24602") // 替换为你的 Kafka 主题
      .option("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      .option("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      .mode("append")
      .save()

    // Handle Clusters data to Kafka
    val newClusterIds = (clusterStartAndEndIds._1 to clusterStartAndEndIds._2).toList
    val clusterData = spark.sparkContext.parallelize(newClusterIds).flatMap { clusterId =>
      // 随机选取三个不同的 feature_ids
      val featureIds = Random.shuffle(1 to 60000).take(3)
      // 当前时间戳
      val currentTimestamp = java.time.LocalDateTime.now().format(java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
      Seq(
        (clusterId.toString, s"""{"cluster_id": $clusterId, "message_type": "add_cluster", "featured_ids": [1, 2, 3], "cluster_label": "", "last_modified_date": "$currentTimestamp"}"""),
        (clusterId.toString, s"""{"cluster_id": $clusterId, "message_type": "update_cluster_feature_ids", "feature_ids": [${featureIds.mkString(",")}}]}"""),
        (clusterId.toString, s"""{"cluster_id": $clusterId, "message_type": "update_cluster_label", "cluster_label": "421022196001027320"}""")
      )
    }

    // 创建 DataFrame，每个元组的第一个元素为 key，第二个为 value
    val clusterDF = spark.createDataFrame(clusterData).toDF("key", "value")

    clusterDF.write
      .format("kafka")
      .option("kafka.bootstrap.servers", "kafka-broker:29092") // 替换为你的 Kafka 服务器
      .option("topic", "cluster_24602") // 替换为你的 Kafka 主题
      .option("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      .option("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      .mode("append")
      .save()
  }
}
